import features_tests.*;
import features_tests.interface_changes.DebitAccount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class MainClass {
    private static final Logger LOGGER = LogManager.getLogger(MainClass.class);

    public static void main(String[] args) {
        //LocalDate date = new LocalDate();
        //System.out.println(date.monthOfYear().getAsText(Locale.ENGLISH));
        //System.out.println("Hello");

        ExceptionHandling.checkedException();
        //ExceptionHandling.uncheckedException();
        ExceptionHandling.multipleCatchBlocks();
        ExceptionHandling.finallyBlock();

        Time.localDate();
        Time.localTime();
        Time.localDateTime();
        Time.zonedDateTime();
        Time.period();
        Time.duration();

        DebitAccount debitAccount = new DebitAccount();
        debitAccount.bankInterface();

        Optionals.optionalOf();
        Optionals.optionalOrElse();
        Optionals.optionalFilter();
        Optionals.optionalMap();

        Streams.stringListToIntegerListWithEvenElements(List.of("1", "2", "3", "4", "5", "6"));

        /*StringMethods.repeatMethod();
        StringMethods.linesMethod();
        StringMethods.isBlankMethod();
        StringMethods.stripMethod();
        StringMethods.stripLeadingMethod();
        StringMethods.stripTrailingMethod();

        ReadFromFile.readFileNewMethod();*/
        LOGGER.error("Logger error message");
        LOGGER.info("Logger info message");
        LOGGER.debug("Logger debug message");
        LOGGER.fatal("Logger fatal message");
        LOGGER.warn("Logger warn message");
    }
}
