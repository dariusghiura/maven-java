package features_tests;

import java.io.*;

public class ExceptionHandling {
    public static void checkedException(){
        File file = new File("notfound.txt");
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }

    public static void uncheckedException(){
        String s = null;
        System.out.println(s.charAt(0));
    }

    public static void multipleCatchBlocks(){
        try {
            String s = null;
            System.out.println(s.charAt(10));
        } catch(ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBounds Exception");
        } catch(Exception e) {
            System.out.println("Exception");
        }
    }

    public static void finallyBlock(){
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile("file.txt", "r");
            randomAccessFile.write(35);
        } catch (IOException e) {
            System.out.println("Exception has occurred");
        }
        finally {
            try {
                randomAccessFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Finally block executed");
        }

    }
}
