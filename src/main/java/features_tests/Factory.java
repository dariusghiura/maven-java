package features_tests;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Map.entry;

public class Factory {
    public static List<Integer> immutableList(){
        List<Integer> immutableList = List.of(1, 2, 3, 4, 5);
        return immutableList;
    }

    public static Set<Integer> immutableSet(){
        Set<Integer> immutableSet = Set.of(1, 2, 3, 4, 5);
        return  immutableSet;
    }

    public static Map<String, Integer> immutableMap(){
        Map<String, Integer> immutableMap = Map.of("a", 1, "b", 2, "c", 3);
        //Daca sunt mai mult de 10 perechi cheie - valoare se foloseste Map.ofEntries()
        return immutableMap;
    }

    public static Map<String, Integer> immutableMapEntry(){
        Map<String, Integer> immutableMap = Map.ofEntries(
            entry("a", 1),
            entry("b", 2),
            entry("c", 3),
            entry("d", 4),
            entry("e", 5),
            entry("f", 6),
            entry("g", 7),
            entry("h", 8),
            entry("i", 9),
            entry("j", 10),
            entry("k", 11),
            entry("l", 12)
        );
        return immutableMap;
    }
}
