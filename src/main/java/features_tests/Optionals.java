package features_tests;

import java.util.Optional;

public class Optionals {
    public static void optionalOf(){
        String s = "Present";
        String s1 = null;
        Optional<String> emptyOptional = Optional.empty();
        System.out.println(emptyOptional.isPresent());

        Optional<String> optional = Optional.of(s);
        optional.ifPresent(System.out::println);

        try{
            Optional<String> optional1 = Optional.of(s1);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
        }
        Optional<String> optional2 = Optional.ofNullable(s1);
        System.out.println(optional2.isPresent());
    }

    public static void optionalOrElse(){
        String s = null;
        String s1 = Optional.ofNullable(s).orElse("Default String");
        System.out.println(s1);
    }

    public static void optionalFilter(){
        String s = "aa";
        Optional<String> optionalLength1 = Optional.of(s);
        boolean isLength1 = optionalLength1.filter(s1 -> s1.length() == 1).isPresent();
        boolean isLength2 = optionalLength1.filter(s2 -> s2.length() == 2).isPresent();

        System.out.println("Length = 1 " + isLength1);
        System.out.println("Length = 2 " + isLength2);
    }

    public static void optionalMap(){
        String s = "codemart";
        Optional<String> optional = Optional.of(s);

        int len = optional
                .map(String::length)
                .orElse(0);
        System.out.println(len);
    }
}
