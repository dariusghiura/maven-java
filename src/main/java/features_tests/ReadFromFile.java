package features_tests;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFromFile {
    public static String readFileOldMethod(String fileName) throws IOException {
        FileReader fileReader = new FileReader(fileName);
        StringBuilder text = new StringBuilder();
        int c;
        while ((c = fileReader.read()) != -1){
            text.append((char)c);
        }
        fileReader.close();

        System.out.println(text);
        return text.toString();
    }

    public static String readFileNewMethod(String fileName) throws IOException{
        Path path = Paths.get(fileName);
        Stream<String> streamOfStrings = null;
        streamOfStrings = Files.lines(path);
        String text = streamOfStrings.collect(Collectors.joining("\n"));
        System.out.println(text);
        return text;
    }
}
