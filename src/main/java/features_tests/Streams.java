package features_tests;

import java.util.List;
import java.util.stream.Collectors;

public class Streams {
    public static List<Integer> stringListToIntegerListWithEvenElements(List<String> stringList){
        List<Integer> result = stringList.stream()
                .map(Integer::valueOf)
                .filter(x -> x % 2 == 0)
                .collect(Collectors.toList());
        for (Integer i : result) {
            System.out.println(i);
        }
        return result;
    }
}
