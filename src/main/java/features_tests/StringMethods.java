package features_tests;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringMethods {
    public static String repeatMethod(String s, int n){
        String s1 = s.repeat(n);
        System.out.println(s1);
        return s1;
    }

    public static List<String> linesMethod(String s){
        Stream<String> strings = s.lines();
        List<String> list = strings.collect(Collectors.toList());
        return list;
    }

    public static boolean isBlankMethod(String s){
        System.out.println(s);
        return s.isBlank();
    }

    public static String stripMethod(String s){
        s = s.strip();
        System.out.println(s);
        return s;
    }

    public static String stripLeadingMethod(String s){
        s = s.stripLeading();
        System.out.println(s);
        return s;
    }

    public static String stripTrailingMethod(String s){
        s = s.stripTrailing();
        System.out.println(s);
        return s;
    }
}
