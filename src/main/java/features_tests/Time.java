package features_tests;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class Time {
    public static void localDate(){
        LocalDate localDate = LocalDate.now();
        System.out.println("Today's date : " + localDate);

        LocalDate pastDate = LocalDate.parse("2018-02-13");
        System.out.println(pastDate);

        LocalDate tomorrow = localDate.plusDays(1);
        System.out.println("Tomorrow : " + tomorrow);

        LocalDate previousMonthSameDay = LocalDate.now().minus(1, ChronoUnit.MONTHS);
        System.out.println("Prev month same day : " + previousMonthSameDay);
    }

    public static void localTime(){
        LocalTime now = LocalTime.now();
        System.out.println("Time is : " + now);

        LocalTime oneHourFromNow = now.plus(1, ChronoUnit.HOURS);
        System.out.println("One hour from now : " + oneHourFromNow);
    }

    public static void localDateTime(){
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("Now : " + localDateTime);

        LocalDateTime tomorrow = localDateTime.plusDays(1);
        System.out.println("Tomorrow : " + tomorrow);
    }

    public static void zonedDateTime(){
        ZoneId zoneId = ZoneId.of("Europe/Paris");
        LocalDateTime localDateTime = LocalDateTime.now();
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);
        System.out.println("Paris Time Zone : " + zonedDateTime);
    }

    public static void period(){
        LocalDate initialDate = LocalDate.now();
        LocalDate finalDate = initialDate.plusDays(4);
        Period period = Period.between(initialDate, finalDate);
        System.out.println("Days between " + initialDate + " and " + finalDate + " : " + period.getDays());
    }

    public static void duration(){
        LocalTime initialTime = LocalTime.now();
        LocalTime finalTime = initialTime.plus(Duration.ofSeconds(30));
        Duration duration = Duration.between(initialTime, finalTime);
        System.out.println("Seconds between " + initialTime + " and " + finalTime + " : " + duration.getSeconds());
    }
}
