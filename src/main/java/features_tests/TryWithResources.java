package features_tests;

import java.io.IOException;
import java.io.RandomAccessFile;

public class TryWithResources {
    public static void tryWithResources(){
        try (RandomAccessFile randomAccessFile = new RandomAccessFile("file.txt", "r")){
            randomAccessFile.write(35);
        } catch (IOException e) {
            System.out.println("Exception has occurred");
        }
    }
}
