package features_tests.interface_changes;

public interface BankAccount {
    default String accountInfo(){
        return "BT Account";
    }

    static double eurToUsd(double eur){
        return eur * 1.18;
    }
}
