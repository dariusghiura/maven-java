package features_tests.interface_changes;

public class DebitAccount implements BankAccount {
    @Override
    public String accountInfo() {
        return "BT Debit Account";
    }

    public void bankInterface(){
        System.out.println(accountInfo());
        double eur = 253.5;
        double usd = BankAccount.eurToUsd(eur);
        System.out.println("Eur : " + eur + " to Usd : " + usd);
    }
}
