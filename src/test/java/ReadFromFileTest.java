import features_tests.ReadFromFile;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class ReadFromFileTest {
    final static String text = "test text file";
    final static String existingFileName = "file.txt";
    final static String nonExistingFileName = "404.txt";
    @Test
    void testOldReadMethodSuccess() throws IOException {
        assertEquals(text, ReadFromFile.readFileOldMethod(existingFileName));
    }

    @Test
    void testOldReadMethodFail(){
        assertThrows(IOException.class, () -> ReadFromFile.readFileOldMethod(nonExistingFileName));
    }

    @Test
    void testNewReadMethodSuccess() throws IOException {
        assertEquals(text, ReadFromFile.readFileNewMethod(existingFileName));
    }

    @Test
    void testNewReadMethodFail(){
        assertThrows(IOException.class, () -> ReadFromFile.readFileNewMethod(nonExistingFileName));
    }
}
