import features_tests.StringMethods;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
public class StringMethodsTest {
    final static String s = "codemart ";
    final static String repeated = "codemart codemart codemart ";
    final static String lines = "Lorem\nIpsum";
    final static String empty = "";
    final static String strip = "   codemart   ";
    final static String stripped = "codemart";
    final static String leadStripped = "codemart   ";
    final static String trailStripped = "   codemart";

    @Test
    void repeatMethodTest(){
        assertEquals(repeated, StringMethods.repeatMethod(s, 3));
    }

    @Test
    void linesMethodTest(){
        List<String> list = List.of("Lorem","Ipsum");
        assertEquals(list, StringMethods.linesMethod(lines));
    }

    @Test
    void isBlankMethodTrue(){
        assertTrue(StringMethods.isBlankMethod(empty));
    }

    @Test
    void isBlankMethodFalse(){
        assertFalse(StringMethods.isBlankMethod(s));
    }

    @Test
    void stripMethodTest(){
        assertEquals(stripped, StringMethods.stripMethod(strip));
    }

    @Test
    void stripLeadingMethodTest(){
        assertEquals(leadStripped, StringMethods.stripLeadingMethod(strip));
    }

    @Test
    void stripTrailingMethodTest(){
        assertEquals(trailStripped, StringMethods.stripTrailingMethod(strip));
    }

}
